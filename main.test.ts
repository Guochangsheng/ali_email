import { AliEmail, AliEmailHost, AliEmailModel } from './main';
const cfg=require('./config.json');
describe('测试触发类邮件（注册激活、密码找回等）',()=>{
    const cliBatch=new AliEmail(AliEmailHost.HuaDong1,465,cfg.emailSingle.username,cfg.emailSingle.password);
    let em=<AliEmailModel>{
        FromNick:'Development 开发者触发类测试',
        To:cfg.sendTo,
        Subject:'Development test singleSend'
    }
    test('发送单个文本类型邮箱',async()=>{
        em.BodyText=`[Single ${Date.now()}] Hello world, this is sent by textbody email!`;
        let spn=jest.spyOn(cliBatch,'Send');
        let res=await cliBatch.Send(em);
        expect(spn).toBeCalled();
        expect(res.response).toBeDefined();
        expect(res.response).toMatch(/250\sData\sOk/i);
    });
    test('发送单个网页类型邮箱',async()=>{
        em.BodyHtml=`<font color="red">[Single ${Date.now()}]</font><h1>Hello world, this is sent by htmlbody email!</h1>`;
        let spn=jest.spyOn(cliBatch,'Send');
        let res=await cliBatch.Send(em);
        expect(spn).toBeCalled();
        expect(res.response).toBeDefined();
        expect(res.response).toMatch(/250\sData\sOk/i);
    });
});
describe('测试批量类邮件（营销推广、订阅期刊等）',()=>{
    const cliBatch=new AliEmail(AliEmailHost.HuaDong1,465,cfg.emailBatch.username,cfg.emailBatch.password);
    let em=<AliEmailModel>{
        FromNick:'Development 开发者批量测试',
        To:cfg.sendToMore,
        Subject:'Development test batchSend'
    }
    test('批量发送文本类型邮箱',async()=>{
        em.BodyText=`[Batch ${Date.now()}] Hello world, this is sent by textbody email!`;
        let spn=jest.spyOn(cliBatch,'Send');
        let res=await cliBatch.Send(em);
        expect(spn).toBeCalled();
        expect(res.response).toBeDefined();
        expect(res.response).toMatch(/250\sData\sOk/i);
    });
    test('批量发送网页类型邮箱',async()=>{
        em.BodyHtml=`<font color="red">[Batch ${Date.now()}]</font><h1>Hello world, this is sent by htmlbody email!</h1>`;
        let spn=jest.spyOn(cliBatch,'Send');
        let res=await cliBatch.Send(em);
        expect(spn).toBeCalled();
        expect(res.response).toBeDefined();
        expect(res.response).toMatch(/250\sData\sOk/i);
    });
});