const nodemailer = require('nodemailer');
export class AliEmail {
    constructor(host: AliEmailHost, port: number, userName: string, password: string) {
        this.userName = userName;
        this.service = nodemailer.createTransport(<any>{
            host: host.toString(),
            port: port,
            secure: port == 465 ? true : false,
            secureConnection: port == 465 ? true : false,
            auth: {
                user: userName,
                pass: password
            }
        });
    };
    private service: any;
    private userName: string;
    async Send(model: AliEmailModel): Promise<any> {
        let em = <any>{
            from: `${model.FromNick ? `${model.FromNick}<${this.userName}>` : this.userName}`,
            to: model.To.join(','),
            subject: model.Subject,
            text: model.BodyText,
            html: model.BodyHtml,
            replyTo: model.ReplyTo,
            attachments: model.AttachMents
        };
        if (model.CC) em.cc = model.CC.join(',');
        if (model.BCC) em.bcc = model.BCC.join(',');
        return new Promise((resolve,reject)=>{
            this.service.sendMail(em, function (err: any, info: any) {
                if (err) reject(err);
                else resolve(info);
            });
        });
        
    };
}
export enum AliEmailHost {
    HuaDong1 = 'smtpdm.aliyun.com',
    Singapore = 'smtpdm-ap-southeast-1.aliyun.com',
    Sydeny = 'smtpdm-ap-southeast-2.aliyun.com'
}
export interface AliEmailModel {
    FromNick?: string;
    To: string[];
    CC?: string[];
    BCC?: string[];
    Subject: string;
    ReplyTo?: string;
    BodyText?: string;
    BodyHtml?: string;
    AttachMents?: Array<{
        filename: string,
        content: string,
        cid?: string
    }>;
}
export interface AliEmailConfig {
    Host: string;
    Port: number;
    SecureConnection?: boolean; /// use SSL, the port is 465
    Auth: {
        UserName: string;
        Password: string;
    }
}